import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;

/**
 * Class description goes here
 *
 * @author Michael A. Alan
 */
public class DriverLoader {
    public static ProvizDevice load(File file){
        try {
            Unmarshaller unmarshaller = JAXBContext.newInstance(ProvizDevice.class).createUnmarshaller();
            return (ProvizDevice) unmarshaller.unmarshal(file);
        }catch(Exception e){
            System.out.println("Error loading driver");
            javax.swing.JOptionPane.showMessageDialog(null,"Error Loading Driver");
        }
        return null;
    }
}
