import java.util.*;

/**
 * Class description goes here
 *
 * @author Michael A. Alan
 */
public class ConnectionMap {
    private TreeMap<PeripheralConnection,Connection> wiringMap = new TreeMap<>();
    private ProvizDevice platform;
    private List<ProvizDevice> peripherals = new LinkedList<>();
    private PeripheralTable peripheralTable = new PeripheralTable();

    public ConnectionMap(){
    }

    public boolean addPeripheral(ProvizDevice device){
        device.setAlias(device.getDeviceName() + " " + peripheralTable.countPeripheral(device.getDeviceName()));
        peripherals.add(device);
        if(attemptWiring()){
            return true;
        }else{
            peripherals.remove(device);
            return false;
        }
    }

    public int resolvePort(String alias, Connection c){
        return wiringMap.get(new PeripheralConnection(alias,c)).getPosition();
    }

    private boolean attemptWiring(){
        LinkedList<Connection> availablePlatformConnection = new LinkedList<>(platform.getConnectionList());
        List<PeripheralConnection> peripheralConnections = new LinkedList<>();
        for(ProvizDevice p : peripherals){
            for(Connection c : p.getConnectionList()){
                peripheralConnections.add(new PeripheralConnection(p.getAlias(),c));
            }
        }
        //Map connections with port preferences
        for(Iterator<PeripheralConnection> pConIterator = peripheralConnections.iterator(); pConIterator.hasNext();){
            PeripheralConnection con = pConIterator.next();
            int requestPin = con.getConnection().getPosition();
            if(requestPin != -1){
                boolean pinAvailable = false;
                for(Iterator<Connection> connectionIterator = availablePlatformConnection.iterator(); connectionIterator.hasNext();){
                    Connection c = connectionIterator.next();
                    if(c.getPosition()==requestPin){
                        wiringMap.put(con,c);
                        connectionIterator.remove();
                        pConIterator.remove();
                        pinAvailable=true;
                        break;
                    }
                }
                if(!pinAvailable){
                    String conflictAlias ="";
                    for(PeripheralConnection pc : wiringMap.keySet()){
                        if(pc.getConnection().getPosition()==requestPin){
                            conflictAlias = pc.alias;
                            break;
                        }
                    }
                    javax.swing.JOptionPane.showMessageDialog(null,"Error: " + con.alias + " and " + conflictAlias + " both require pin " + requestPin);
                    return false;
                }
            }
        }

        //sort remaining platform connections into functional categories
        LinkedList<Connection> availableDigitalPorts = new LinkedList<>();
        LinkedList<Connection> availableAnalogIOPorts = new LinkedList<>();
        LinkedList<Connection> availableAnalogInPorts = new LinkedList<>();
        LinkedList<Connection> availableAnalogOutPorts = new LinkedList<>();
        for(Connection c : availablePlatformConnection){
            if (c.getAnalogIn() && c.getAnalogOut()) {
                availableAnalogIOPorts.offer(c);
            } else if (c.getAnalogIn()) {
                availableAnalogInPorts.offer(c);
            } else if (c.getAnalogOut()) {
                availableAnalogOutPorts.offer(c);
            } else {
                availableDigitalPorts.offer(c);
            }
        }

        //map peripheral connections into platform connections, keeping track of overflowed digital connections to map them last
        LinkedList<PeripheralConnection> digitalOverflow = new LinkedList<>();

        for(PeripheralConnection pc : peripheralConnections){
            Connection connectionToMap = null;
            if(pc.getConnection().getAnalogIn() && pc.getConnection().getAnalogOut()) {
                if (!availableAnalogIOPorts.isEmpty()) {
                    connectionToMap = availableAnalogIOPorts.poll();
                } else {
                    javax.swing.JOptionPane.showMessageDialog(null, "Not Enough Analog IO ports");
                    return false;
                }
            }else if(pc.getConnection().getAnalogIn()) {
                if(!availableAnalogInPorts.isEmpty()){
                    connectionToMap = availableAnalogInPorts.poll();
                }else if (!availableAnalogIOPorts.isEmpty()) {
                    connectionToMap = availableAnalogIOPorts.poll();
                }else {
                    System.out.println("Not enough Analog In ports");
                    javax.swing.JOptionPane.showMessageDialog(null, "Not Enough Analog In ports");
                    return false;
                }
            }else if(pc.getConnection().getAnalogOut()) {
                if(!availableAnalogOutPorts.isEmpty()){
                    connectionToMap = availableAnalogOutPorts.poll();
                }else if (!availableAnalogIOPorts.isEmpty()) {
                    connectionToMap = availableAnalogIOPorts.poll();
                }else {
                    System.out.println("Not enough Analog In ports");
                    javax.swing.JOptionPane.showMessageDialog(null, "Not Enough Analog Out ports");
                    return  false;
                }
            }else{
                if (!availableDigitalPorts.isEmpty()){
                    connectionToMap = availableDigitalPorts.poll();
                }else{
                    digitalOverflow.add(pc);
                }
            }

            if(connectionToMap != null) {
                wiringMap.put(pc,connectionToMap);
            }
        }

        //map digital overflow
        for(PeripheralConnection overflownDigitalConnection : digitalOverflow){
            if (!availableAnalogInPorts.isEmpty()){
                wiringMap.put(overflownDigitalConnection,availableAnalogInPorts.poll());
            }else if(!availableAnalogOutPorts.isEmpty()){
                wiringMap.put(overflownDigitalConnection,availableAnalogOutPorts.poll());
            }else if(!availableAnalogIOPorts.isEmpty()){
                wiringMap.put(overflownDigitalConnection,availableAnalogIOPorts.poll());
            }else{
                javax.swing.JOptionPane.showMessageDialog(null, "Not Enough Platform Connections");
                return false;
            }
        }
        return true;
    }

    @Override
    public String toString() {
        if(wiringMap.size()==0){
            return "Connection Map is Empty";
        }else{
            return ""; //TODO: wrong
        }
    }

    public class PeripheralConnection implements Comparable{
        private String alias;
        private Connection connection;
        public PeripheralConnection(String alias, Connection connection){
            this.alias = alias;
            this.connection = connection;
        }

        public String getAlias() {
            return alias;
        }

        public void setAlias(String alias) {
            this.alias = alias;
        }

        public Connection getConnection() {
            return connection;
        }

        public void setC(Connection connection) {
            this.connection = connection;
        }

        @Override
        public boolean equals(Object o) {
            PeripheralConnection that = (PeripheralConnection)o;
            return (this.alias + this.connection.getName()).equals(that.alias + that.getConnection().getName());
        }


        @Override
        public int compareTo(Object o) {
            PeripheralConnection that = (PeripheralConnection) o;
            return (this.alias + this.connection.getName()).compareTo(that.alias + that.getConnection().getName());
        }
    }

    public TreeMap<PeripheralConnection, Connection> getWiringMap() {
        return wiringMap;
    }

    public void setWiringMap(TreeMap<PeripheralConnection, Connection> wiringMap) {
        this.wiringMap = wiringMap;
    }

    public ProvizDevice getPlatform() {
        return platform;
    }

    public void setPlatform(ProvizDevice platform) {
        this.platform = platform;
    }

    public List<ProvizDevice> getPeripherals() {
        return peripherals;
    }

    public void setPeripherals(List<ProvizDevice> peripherals) {
        this.peripherals = peripherals;
    }

    private class PeripheralTable{
        private TreeMap<String,Integer> peripheralTable = new TreeMap<>();
        public int countPeripheral(String name){
            int r = (peripheralTable.get(name)==null) ? 0 : (peripheralTable.get(name)+1);
            peripheralTable.put(name,r);
            return r;
        }
    }

}
