import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.List;

/**
 * Class description goes here
 *
 * @author Michael A. Alan
 */
public class MainGUI {
    private JPanel mainPanel;
    private JTabbedPane tabbedPane1;
    private JPanel TopologyPane;
    private JPanel DataPane;
    private JPanel buildePanel;
    private JToggleButton ArduinoBtn;
    private JToggleButton RPiBtn;
    private JToggleButton BBBtn;
    private JComboBox boardComboBox;
    private JButton addSensorBtn;
    private JButton addRadioBtn;
    private JButton createNodeButton;
    private JPanel comboPanel;
    private JPanel sensorPanel;
    private JPanel radioPanel;
    private JPanel sensorIconPanel;
    private JPanel radioIconPanel;
    private static JFrame frame;
    private ConnectionMap node;

    public MainGUI() {
        //TODO: DEMO CODE, KILL WITH FIRE LATER
        node = new ConnectionMap();
        System.out.println("Is it okay to check the platform box now? " +((ProvizDevice)boardComboBox.getSelectedItem()).getDeviceName());
        node.setPlatform((ProvizDevice) boardComboBox.getSelectedItem());
        //TODO: END DEMO CODE
        addSensorBtn.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                PeripheralSelector sensorSelector = new PeripheralSelector("Arduino", "Sensor", frame);
                if(sensorSelector.getSelectedDevice()!=null) {
                    ProvizDevice selectedDevice = sensorSelector.getSelectedDevice();
                    if(node.addPeripheral(selectedDevice)){
                        JButton sensor = new JButton(new ImageIcon(selectedDevice.getImage()));
                        sensor.setBorder(null);
                        sensorIconPanel.add(sensor);
                    }
                    frame.setVisible(true);
                }
            }
        });
        addRadioBtn.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                PeripheralSelector sensorSelector = new PeripheralSelector("Arduino", "Radio", frame);
                if(sensorSelector.getSelectedDevice()!=null) {
                    ProvizDevice selectedDevice = sensorSelector.getSelectedDevice();
                    if(node.addPeripheral(selectedDevice)) {
                        JButton radio = new JButton(new ImageIcon(selectedDevice.getImage()));
                        radio.setBorder(null);
                        radioIconPanel.add(radio);
                    }
                    frame.setVisible(true);
                }
            }
        });
        createNodeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println(ArduinoLinker.buildSource(node));
            }
        });
    }

    public static void main(String[] args) {
        try{UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());}catch(Exception e){e.printStackTrace();}

        frame = new JFrame("Proviz2");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        MainGUI gui = new MainGUI();
        frame.setJMenuBar(gui.createMenuBar());
        JTabbedPane tabbedPane = new JTabbedPane();
        frame.setContentPane(gui.mainPanel);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    public JMenuBar createMenuBar() {
        JMenuBar menuBar = new JMenuBar();

        JMenu fileMenu = new JMenu("File");
        fileMenu.setMnemonic(KeyEvent.VK_F);
        JMenuItem menuItemNew = new JMenuItem("New", KeyEvent.VK_N);
        fileMenu.add(menuItemNew);
        JMenuItem menuItemOpen = new JMenuItem("Open", KeyEvent.VK_O);
        fileMenu.add(menuItemOpen);
        fileMenu.addSeparator();
        JMenuItem menuItemSave = new JMenuItem("Save", KeyEvent.VK_S);
        fileMenu.add(menuItemSave);
        fileMenu.addSeparator();
        JMenuItem menuItemPrint = new JMenuItem("Print", KeyEvent.VK_P);
        fileMenu.add(menuItemPrint);
        fileMenu.addSeparator();
        JMenuItem menuItemExit = new JMenuItem("Exit", KeyEvent.VK_E);
        fileMenu.add(menuItemExit);
        menuBar.add(fileMenu);

        JMenu editMenu = new JMenu("Edit");
        editMenu.setMnemonic(KeyEvent.VK_E);
        JMenuItem menuItemPref = new JMenuItem("Preferences", KeyEvent.VK_P);
        editMenu.add(menuItemPref);
        menuBar.add(editMenu);

        JMenu toolsMenu = new JMenu("Tools");
        toolsMenu.setMnemonic(KeyEvent.VK_T);
        JMenuItem menuItemTools = new JMenuItem("Driver Utility", KeyEvent.VK_P);
        menuItemTools.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new DriverCreator();
            }
        });
        toolsMenu.add(menuItemTools);
        menuBar.add(toolsMenu);


        JMenu helpMenu = new JMenu("Help");
        helpMenu.setMnemonic(KeyEvent.VK_H);
        JMenuItem menuItemAbout = new JMenuItem("About",KeyEvent.VK_A);
        helpMenu.add(menuItemAbout);
        menuBar.add(helpMenu);

        return menuBar;
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
        boardComboBox = new JComboBox();
        boardComboBox.setRenderer(new ListCellRenderer() {
            @Override
            public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
                JLabel cell = new JLabel();
                if(value instanceof ProvizDevice){
                    ProvizDevice device = (ProvizDevice)value;
                    cell.setIcon(new ImageIcon(device.getImage()));
                    cell.setText(device.getDeviceName());
                    cell.setHorizontalTextPosition(JLabel.CENTER);
                    cell.setVerticalTextPosition(JLabel.BOTTOM);
                    cell.setPreferredSize(new Dimension(0,148));
                }
                //System.out.println(cell.getPreferredSize()+ ":"+cell.getMinimumSize()+":"+cell.getMaximumSize());

                return cell;
            }
        });
        for(ProvizDevice device : this.getSupportedDevices("Arduino","Platform")){
            boardComboBox.addItem(device);
        }
        ButtonGroup platforms = new ButtonGroup();
        ArduinoBtn = new JToggleButton();
        RPiBtn = new JToggleButton();
        BBBtn = new JToggleButton();
        platforms.add(ArduinoBtn);
        platforms.add(RPiBtn);
        platforms.add(BBBtn);
        ArduinoBtn.setSelected(true);

        sensorIconPanel = new JPanel();
        radioIconPanel = new JPanel();
        sensorIconPanel.setLayout(new BoxLayout(sensorIconPanel,BoxLayout.PAGE_AXIS));
        radioIconPanel.setLayout(new BoxLayout(radioIconPanel,BoxLayout.PAGE_AXIS));
    }

    private java.util.List<ProvizDevice> getSupportedDevices(String platform, String type){
        List<ProvizDevice> supportedDevices = new LinkedList<>();
        try {
            Files.walk(Paths.get("C:\\Users\\Micha\\IdeaProjects\\Proviz2\\Resources\\Drivers\\"+platform+"\\"+type+"s")).forEach(filePath -> {
                if (Files.isRegularFile(filePath)) {
                    supportedDevices.add(DriverLoader.load(filePath.toFile()));
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }

        return supportedDevices;
    }
}
