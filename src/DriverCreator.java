import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.tree.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.List;

/**
 * Graphical utility for creating and editing XML drivers.
 *
 * @author Michael A. Alan
 */
public class DriverCreator {
    private JPanel mainPanel;
    private JPanel fieldsPanel;
    private JLabel driverTypeLabel;
    private JComboBox typeComboBox;
    private JLabel platformLabel;
    private JComboBox platformComboBox;
    private JLabel nameLabel;
    private JTextField nameField;
    private JLabel librariesLabel;
    private JTextField librariesField;
    private JLabel portsLabel;
    private JTextField portsField;
    private JLabel codeLabel;
    private JScrollPane codeScrollPane;
    private JTextArea codeTextArea;
    private JButton exportBtn;
    private JPanel treePanel;
    private JScrollPane treeScrollPane;
    private JTree tree;
    private JButton browseButton;
    private IconPanel iconPanel;

    public DriverCreator() {
        librariesField.setEditable(false);
        typeComboBox.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                switch((String)e.getItem()){
                    case "Platform":
                        librariesField.setEditable(false);
                        break;
                    case "Peripheral":
                        librariesField.setEditable(true);
                        break;
                }
            }
        });
        exportBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ProvizDevice device = new ProvizDevice("", nameField.getText(), (String)platformComboBox.getSelectedItem(), (String)typeComboBox.getSelectedItem(), librariesField.getText(), codeTextArea.getText(), iconPanel.getImage(), portsField.getText()); //TODO: fix fields
                DriverWriter.write(device);
                tree.setModel(new FileSystemModel(new File(System.getProperty("user.dir")+"\\Resources\\")));
                expandTree(tree);
                nameField.setText("");
                librariesField.setText("");
                portsField.setText("");
                codeTextArea.setText("");
                //iconPanel.setImage();
                //TODO: probably need to clear icons and stuff too
            }
        });

        JFrame frame = new JFrame("Proviz2 Driver Editor");
        frame.setContentPane(mainPanel);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        browseButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                final JFileChooser fc = new JFileChooser();
                fc.setFileFilter(new FileNameExtensionFilter("PNG","png"));
                if(JFileChooser.APPROVE_OPTION==fc.showOpenDialog(null)) {
                    try {
                        iconPanel.setImage(ImageIO.read(fc.getSelectedFile()));
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        });
    }

    private java.util.List<String> parseLibraries(String libraries){
        List<String> r = new LinkedList<>();
        Scanner s = new Scanner(libraries);
        s.useDelimiter(",");
        while (s.hasNext()) {
            r.add(s.next());
        }
        return r;
    }

    private void createUIComponents() {
        File path = new File("C:\\Users\\Micha\\IdeaProjects\\Proviz2\\Resources\\");
        tree = new JTree(new FileSystemModel(path));
        tree.setEditable(false);
        tree.setRootVisible(false);
        expandTree(tree);
        tree.addTreeSelectionListener(event -> {
            File file = (File) tree.getLastSelectedPathComponent();
            if(file != null) {
                System.out.println(file);
                if (file.getPath().toLowerCase().contains("xml")) {
                    loadDriver(file);
                    if (!file.getPath().toLowerCase().contains("platform")) {
                        librariesField.setEditable(true);
                    } else{
                        librariesField.setEditable(false);
                    }
                }
            }
        });
    }

    private void loadDriver(File path){
        if(!((nameField.getText().isEmpty()) && (portsField.getText().isEmpty()) && (librariesField.getText().isEmpty()) && (codeTextArea.getText().isEmpty()))){
            Object[] options = {"Yes","No"};
            if(JOptionPane.NO_OPTION == JOptionPane.showOptionDialog(null,"Load stored driver and abandon current driver?","Warning",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE,null,options,options[1])){
                return;
            }
        }
        ProvizDevice device = DriverLoader.load(path);
        typeComboBox.setSelectedItem(device.getDeviceType());
        platformComboBox.setSelectedItem(device.getPlatform() == null ? "" : device.getPlatform());
        nameField.setText(device.getDeviceName() == null ? "" : device.getDeviceName());
        librariesField.setText(device.getLibraries()==null ? "" : device.getLibrariesString());
        portsField.setText(device.getDriverPinMap() == null ? "" : device.getDriverPinMap());
        codeTextArea.setText(device.getCode() == null ? "" : device.getCode());
        iconPanel.setImage(device.getImage());
    }

    private void expandTree(JTree tree){
        int row = 0;
        while (row < tree.getRowCount()) {
            tree.expandRow(row);
            row++;
        }
    }

    class FileSystemModel implements TreeModel {
        private File root;

        private Vector listeners = new Vector();

        public FileSystemModel(File rootDirectory) {
            root = rootDirectory;
        }

        public Object getRoot() {
            return root;
        }

        public Object getChild(Object parent, int index) {
            File directory = (File) parent;
            String[] children = directory.list();
            return new TreeFile(directory, children[index]);
        }

        public int getChildCount(Object parent) {
            File file = (File) parent;
            if (file.isDirectory()) {
                String[] fileList = file.list();
                if (fileList != null)
                    return file.list().length;
            }
            return 0;
        }

        public boolean isLeaf(Object node) {
            File file = (File) node;
            return file.isFile();
        }

        public int getIndexOfChild(Object parent, Object child) {
            File directory = (File) parent;
            File file = (File) child;
            String[] children = directory.list();
            for (int i = 0; i < children.length; i++) {
                if (file.getName().equals(children[i])) {
                    return i;
                }
            }
            return -1;

        }

        public void valueForPathChanged(TreePath path, Object value) {
            File oldFile = (File) path.getLastPathComponent();
            String fileParentPath = oldFile.getParent();
            String newFileName = (String) value;
            File targetFile = new File(fileParentPath, newFileName);
            oldFile.renameTo(targetFile);
            File parent = new File(fileParentPath);
            int[] changedChildrenIndices = { getIndexOfChild(parent, targetFile) };
            Object[] changedChildren = { targetFile };
            fireTreeNodesChanged(path.getParentPath(), changedChildrenIndices, changedChildren);

        }

        private void fireTreeNodesChanged(TreePath parentPath, int[] indices, Object[] children) {
            TreeModelEvent event = new TreeModelEvent(this, parentPath, indices, children);
            Iterator iterator = listeners.iterator();
            TreeModelListener listener = null;
            while (iterator.hasNext()) {
                listener = (TreeModelListener) iterator.next();
                listener.treeNodesChanged(event);
            }
        }

        public void addTreeModelListener(TreeModelListener listener) {
            listeners.add(listener);
        }

        public void removeTreeModelListener(TreeModelListener listener) {
            listeners.remove(listener);
        }

        private class TreeFile extends File {
            public TreeFile(File parent, String child) {
                super(parent, child);
            }

            public String toString() {
                return getName();
            }
        }
    }
}