import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;

/**
 * Class description goes here
 *
 * @author Michael A. Alan
 */
public class DriverWriter {
    //TODO: add jar support here
    public static void write(ProvizDevice device){

        File file = new File(System.getProperty("user.dir")+"\\Resources\\Drivers\\" + device.getPlatform() + "\\"+device.getDeviceType()+"s\\" + device.getDeviceName() + ".xml");  //TODO: add logic for ide vs jar execution
        try {
            Marshaller marshaller = JAXBContext.newInstance(ProvizDevice.class).createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(device, new FileOutputStream(file));
        } catch (JAXBException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
