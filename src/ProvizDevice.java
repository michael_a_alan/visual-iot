import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.awt.*;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

/**
 * Class description goes here
 *
 * @author Michael A. Alan
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.NONE)
public class ProvizDevice {
    @XmlElement
    private String alias;
    @XmlElement
    private String deviceName;
    @XmlElement
    private String platform;
    @XmlElement
    private String deviceType;
    @XmlElement
    private String code;
    @XmlElement
    private Image Image;
    @XmlElement
    private List<Connection> connectionList;
    @XmlElement
    private List<String> libraries;

    public ProvizDevice(){}

    public ProvizDevice(String alias, String deviceName, String platform, String deviceType, String libraryListString, String code, Image Image, String driverPinString) {
        this.alias = alias;
        this.deviceName = deviceName;
        this.platform = platform;
        this.deviceType = deviceType;
        this.libraries= Arrays.asList(libraryListString.split(","));
        this.code = code;
        this.Image = Image;
        this.connectionList = this.setConnectionsFromString(driverPinString);
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Image getImage() {
        return Image;
    }

    public void setImage(Image image) {
        this.Image = image;
    }

    public List<Connection> getConnectionList() {
        return connectionList;
    }

    public void setConnectionList(List<Connection> connectionList) {
        this.connectionList = connectionList;
    }

    public List<String> getLibraries() {
        return libraries;
    }

    public String getLibrariesString() {
        if(libraries == null){
            return null;
        }
        String r = "";
        for(String l : libraries){
            if(r!=""){
                r+=",";
            }
            r+=l;
        }
        return r;
    }

    public void setLibraries(List<String> libraries) {
        this.libraries = libraries;
    }

    public String getDriverPinMap(){
        if(connectionList == null)
            return null;

        String r = "";
        for(Connection c : connectionList){
            if(!r.equals("")){
                r+=";";
            }
            if(this.getDeviceType().equalsIgnoreCase("platform")){
                r+=c.getIntType();
            }else{
                r += c.getName()+","+c.getIntType()+(c.getPosition()==-1?"":","+c.getPosition());
            }
        }
        return r;
    }

    private List<Connection> setConnectionsFromString(String driverPinString){
        List<Connection> r = new LinkedList<>();
        Scanner conDefs = new Scanner(driverPinString);
        conDefs.useDelimiter(";");
        int position = 0;
        while (conDefs.hasNext()) {
            Connection con = new Connection();
            String[] conDefDetails = conDefs.next().split(",");
            switch(conDefDetails.length){
                case 1:
                    con.setName("");
                    con.setIntType(Integer.parseInt(conDefDetails[0]));
                    con.setPosition(position);
                    break;
                case 2:
                    con.setName(conDefDetails[0]);
                    con.setIntType(Integer.parseInt(conDefDetails[1]));
                    con.setPosition(-1);
                    break;
                case 3:
                    con.setName(conDefDetails[0]);
                    con.setIntType(Integer.parseInt(conDefDetails[1]));
                    con.setPosition(Integer.parseInt(conDefDetails[2]));
                    break;
            }
            r.add(con);
            position++;
        }
        return r;
    }

    @Override
    public String toString() {
        return deviceType + ": " + platform + " " + deviceName + " \"" + alias + "\" " +  connectionList;
    }
}
