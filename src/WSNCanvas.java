import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Line2D;
import java.util.*;

/**
 * Class description goes here
 *
 * @author Michael A. Alan
 */
public class WSNCanvas extends JPanel {
    private int movingNode=-1;
    java.util.List<NodeGraphic> nodes = new LinkedList<>();
    Line2D.Double line = new Line2D.Double();
    JViewport viewPort;
    public WSNCanvas() {
        super.setBackground(Color.white);
        //super.setBorder(BorderFactory.createLoweredBevelBorder());
        super.setSize(500,500);
        nodes.add(new NodeGraphic(0,50,50));
        nodes.add(new NodeGraphic(1,20,20));
        line.setLine(nodes.get(0).getX(),nodes.get(0).getY(),nodes.get(1).getX(),nodes.get(1).getY());

        addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                System.out.println("pressed");
                int x = e.getX();
                int y = e.getY();
                for(int i=0; i < nodes.size(); i++){
                    System.out.println("inside square");
                    NodeGraphic n = nodes.get(i);
                    final int NODE_X = n.getX();
                    final int NODE_Y = n.getY();
                    final int NODE_W = n.getWidth();
                    final int NODE_H = n.getHeight();
                    if((x>NODE_X)&&(x<(NODE_X + NODE_W))&&(y>NODE_Y)&&(y<(NODE_Y+NODE_H))){
                        movingNode = i;
                    }
                }
            }
            @Override
            public void mouseReleased(MouseEvent e) {
                System.out.println("released");
                movingNode = -1;
            }
        });

        addMouseMotionListener(new MouseAdapter() {
            @Override
            public void mouseDragged(MouseEvent e){
                System.out.println("dragged");
                moveSquare(e.getX(),e.getY());
                line.setLine(nodes.get(0).getX(),nodes.get(0).getY(),nodes.get(1).getX(),nodes.get(1).getY());
            }
        });
    }

    private void moveSquare(int x, int y) {
        if(movingNode == -1){
            return;
        }
        NodeGraphic n = nodes.get(movingNode);
        final int NODE_X = n.getX();
        final int NODE_Y = n.getY();
        final int NODE_W = n.getWidth();
        final int NODE_H = n.getHeight();
        final int OFFSET = 1;
        if((NODE_X != x)||(NODE_Y != y)) {
            //if((x>viewPort.getX())&&(y>viewPort.getY())&&(x<(viewPort.getX()+viewPort.getWidth()))&&(x<(viewPort.getY()+viewPort.getHeight()))){
                // The square is moving, repaint background
                // over the old square location.
                repaint(NODE_X, NODE_Y, NODE_W + OFFSET, NODE_H + OFFSET);

                // Update coordinates.
                n.setX(x);
                n.setY(y);

                // Repaint the square at the new location.
                repaint(n.getX(), n.getY(),
                        n.getWidth() + OFFSET,
                        n.getHeight() + OFFSET);

                repaint(line.getBounds());
           // }
        }
    }
    @Override
    public Dimension getPreferredSize() {
        int extentsX = 500;
        int extentsY = 500;
        for(NodeGraphic ng : nodes){
            if((ng.getX()+ng.getWidth()) > extentsX)
                extentsX = ng.getX();
            if((ng.getX()+ng.getHeight()) > extentsY)
                extentsY = ng.getY();
        }
        return new Dimension(extentsX,extentsY);
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        //g.drawString("This is my custom Panel!",10,20);
        for(NodeGraphic n : nodes){
            n.paintSquare(g);
        }
        ((Graphics2D)g).draw(line);
    }
}
