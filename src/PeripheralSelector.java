import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

/**
 * Class description goes here
 *
 * @author Michael A. Alan
 */
public class PeripheralSelector {
    private JPanel mainPanel;
    private ProvizDevice selectedDevice;
    private String type,platform;
    private JDialog frame;

    public PeripheralSelector(String platform,String type, Frame parentFrame) {
        this.platform = platform;
        this.type = type;

        mainPanel = new JPanel(new GridLayout(0,4));
        populateSelector();

        frame = new JDialog(parentFrame, (type.equalsIgnoreCase("sensor") ? "Sensor Slector" : "Radio Selector"), true);
        frame.getContentPane().add(mainPanel);
        frame.setLocationRelativeTo(null);
        frame.setModal(true);
        frame.pack();
        frame.setVisible(true);
    }

    public ProvizDevice getSelectedDevice(){
        return selectedDevice;
    }

    private void populateSelector(){
        try {
            Files.walk(Paths.get("C:\\Users\\Micha\\IdeaProjects\\Proviz2\\Resources\\Drivers\\"+platform+"\\"+type+"s")).forEach(filePath -> {
                if (Files.isRegularFile(filePath)) {
                    ProvizDevice device = DriverLoader.load(filePath.toFile());
                    JButton btn = new JButton(device.getDeviceName(),new ImageIcon(device.getImage()));
                    btn.setVerticalTextPosition(JButton.BOTTOM);
                    btn.setHorizontalTextPosition(JButton.CENTER);
                    btn.addMouseListener(new MouseAdapter() {
                        @Override
                        public void mouseClicked(MouseEvent e) {
                            super.mouseClicked(e);
                            selectedDevice = device;
                            frame.dispose();
                        }
                    });
                    mainPanel.add(btn);
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
