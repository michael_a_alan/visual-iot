import java.util.regex.Matcher;
import java.util.regex.Pattern;
/**Creates Arduino source from ConnectionMap object**/
public class ArduinoLinker {
    private static String SETUP_FUNC = "setup{";
    private static String READ_FUNC = "read{";
    private static String SEND_FUNC = "send{";
    private static String SEND_KEY = "<SEND>";
    private static String DATA_KEY = "<DATA>";

    public static String buildSource(ConnectionMap wiringMap){
        int readRoutineCount = 0;
        int sendRoutineCount = 0;

        String libraries = "";
        String setup = "";
        String subRoutines = "";
        String readRoutine = "String readSensors(){\n";
        String sendRoutine = "void sendData(){\n";

        //get code for each peripheral device
        for(ProvizDevice device : wiringMap.getPeripherals()){
            //build lib string
            for(String lib : device.getLibraries()){
                if(!lib.isEmpty()) {
                    libraries += "#include <" + lib + ">\n";
                }
            }
            //build setup string
            String setupRoutine=loadSubroutine(SETUP_FUNC,device,wiringMap)+"\n";
            if(!setupRoutine.isEmpty()){
                setup+=setupRoutine;
            }
            //add read sub routine string
            String loadedReadRoutine=loadSubroutine(READ_FUNC,device,wiringMap);
            if(!loadedReadRoutine.isEmpty()) {
                subRoutines += ("int read" + readRoutineCount + "(){\n");//TODO:better return type?
                subRoutines += loadedReadRoutine+"}\n\n";
                readRoutineCount++;
            }
            //add send sub routine string
            String loadedSendRoutine=loadSubroutine(SEND_FUNC,device,wiringMap);
            if(!loadedSendRoutine.isEmpty()){
                subRoutines += ("void send" + sendRoutineCount + "(String stringData){\n");
                subRoutines += loadedSendRoutine+"}\n\n";
                sendRoutineCount++;
            }
        }

        //add read routine to call read subroutines
        readRoutine+="String readings=\"\";\n";
        for(int i=0;i<readRoutineCount;i++){
            if(i!=0){
                readRoutine+="readings+=\":\";\n";
            }
            readRoutine+="readings+=read"+i+"();\n";
        }
        readRoutine+="return readings;\n}\n\n";

        //add send routine to call send subroutines
        sendRoutine+="String data = readSensors();\n";
        for(int i=0;i<sendRoutineCount;i++){
            sendRoutine+="send"+i+"(data);\n";
        }
        sendRoutine+="}\n\n";

        //insert compiled strings into platform source
        StringBuilder sourceBuilder = new StringBuilder(wiringMap.getPlatform().getCode());
        sourceBuilder.insert(0,libraries);
        sourceBuilder.insert(sourceBuilder.indexOf("setup(){")+"setup(){".length(),setup);
        sourceBuilder.append("\n\n"+sendRoutine);
        sourceBuilder.append(readRoutine);
        sourceBuilder.append(subRoutines.replaceAll(DATA_KEY,"stringData"));
        return sourceBuilder.toString().replaceAll(SEND_KEY,"sendData();");
    }

    private static String loadSubroutine(String type, ProvizDevice device, ConnectionMap map){
        String routineCode = "";
        int i = device.getCode().indexOf(type);
        if(i!=-1) {
            i += SETUP_FUNC.length();
        }else{
            return "";
        }
        while(device.getCode().charAt(i) != '}'){
            routineCode+=device.getCode().charAt(i);
            i++;
        }

        Pattern varPattern = Pattern.compile("(#\\w+#)");
        Matcher varPatternMatcher = varPattern.matcher(routineCode);
        while(varPatternMatcher.find()) {
            String token = varPatternMatcher.group(1);  //TODO: why 1? idk could be a problem
            for(Connection con : device.getConnectionList()){
                if(con.getName().equals(token.substring(1,token.length()-1))){
                    int pinAssignment = map.resolvePort(device.getAlias(),con);
                    routineCode = routineCode.replaceAll(token, Integer.toString(pinAssignment));  //TODO: should throw user error if var not found
                    break; //TODO store looked up vars as we go instead of looking each one up
                }
            }
        }
        return routineCode;
    }
}