import javax.swing.*;
import java.awt.*;

/**
 * Class description goes here
 *
 * @author Michael A. Alan
 */

//TODO: deal with blank images
public class IconPanel extends JLabel{
    Image image;
    public IconPanel(){}
    public IconPanel(Image image,String text){
        this.image=image;
        super.setIcon(new ImageIcon(image));
        super.setHorizontalTextPosition(JLabel.CENTER);
        super.setVerticalTextPosition(JLabel.BOTTOM);
        super.setText(text);
    }
    public IconPanel(Image image){
        this.image=image;
        super.setIcon(new ImageIcon(image));
    }
    public Image getImage(){
        return image;
    }
    public void setImage(Image image){
        this.image=image;
        super.setIcon(new ImageIcon(image));
    }
    public void setText(String text){
        super.setText(text);
    }
}
