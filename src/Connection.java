import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Class description goes here
 *
 * @author Michael A. Alan
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.NONE)
public class Connection {
    @XmlElement
    private Boolean digitalIn;
    @XmlElement
    private Boolean digitalOut;
    @XmlElement
    private Boolean analogIn;
    @XmlElement
    private Boolean analogOut;
    @XmlElement
    private String name;
    @XmlElement
    private int position;
    public Connection(int typeBitmap, String name, int position) {
        this.setIntType(typeBitmap);
        this.name = name;
        this.position = position;
    }

    public Connection(){}

    public Boolean getDigitalIn() {
        return digitalIn;
    }

    public void setDigitalIn(Boolean digitalIn) {
        this.digitalIn = digitalIn;
    }

    public Boolean getDigitalOut() {
        return digitalOut;
    }

    public void setDigitalOut(Boolean digitalOut) {
        this.digitalOut = digitalOut;
    }

    public Boolean getAnalogIn() {
        return analogIn;
    }

    public void setAnalogIn(Boolean analogIn) {
        this.analogIn = analogIn;
    }

    public Boolean getAnalogOut() {
        return analogOut;
    }

    public void setAnalogOut(Boolean analogOut) {
        this.analogOut = analogOut;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getIntType(){
        return (digitalIn?1:0)+(digitalOut?2:0)+(analogIn?4:0)+(analogOut?8:0);
    }

    public void setIntType(int type){
        digitalIn = (type&1)!=0;
        digitalOut = (type&2)!=0;
        analogIn = (type&4)!=0;
        analogOut = (type&8)!=0;
    }

    @Override
    public String toString() {
        return name + ", " + position + "," + (digitalIn ? " DI":"") + (digitalOut ? " DO":"") + (analogIn ? " AI" : "") + (analogOut ? " AO" : "");
    }
}
